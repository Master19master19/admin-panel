<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Number;
use App\Http\Requests\NumberRequest;

class NumberController extends Controller
{
    public function __construct()
    {
        $this->middleware( 'auth' );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $numbers = Number::all();
        return view( 'numbers.index' , compact( 'numbers' ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	return view( 'numbers.create' );
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( NumberRequest $request)
    {
    	$validated = $request -> validated();
    	if ( $validated ) {
    		Number::create( $validated );
    		return redirect() -> route( 'numbers.index' );
    	}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    	$number = Number::findOrFail( $id );
    	return view( 'numbers.edit' , compact( 'number' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NumberRequest $request, $id)
    {
        //
    	$validated = $request -> validated();
    	if ( $validated ) {
    		if ( ! $request -> has( 'active' ) ) $validated[ 'active' ] = 0;
    		else $validated[ 'active' ] = 1; 
    		Number::where( 'id' , $id ) -> update( $validated );
    		return redirect() -> route( 'numbers.index' );
    	}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$number = Number::findOrFail( $id );
    	$number -> delete();
    	return redirect() -> back();
        //
    }
}
