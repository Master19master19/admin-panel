<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NumberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:70|min:4',
            'number' => 'required|max:25|min:7',
        ];
    }
    // public function messages()
    // {
    //     return [
    //         'name.max' => 'A title is required',
    //         'number.required'  => 'A message is required',
    //     ];
    // }
}
