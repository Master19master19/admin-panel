<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Number extends Model
{
    protected $guarded = [];
	protected $casts = [
		'active' => 'boolean',
	];
	public function getIsActiveAttribute() {
		return $this -> active ? __( 'Yes' ) : __( 'No' ) ;
	}
	public function scopeActive( $q ) {
		return $q -> where( 'active' , 1 );
	}

	public function getActive() {
		return $this -> active() ->get();
	}

}
