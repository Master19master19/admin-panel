@extends( 'layouts.app' )

@section( 'content' )
<div class="container">
    <h1 class="text-center">
        {{ __( 'Create number' ) }}
    </h1>
    @include( 'partials.errors' )
    <form method="POST" action="{{ route( 'numbers.store' ) }}" class="form">
        @csrf
        <div class="form-group">
            <label>{{ __( 'Name' ) }}</label>
            <input type="text" required="" autofocus="" value="{{ old( 'name' ) }}" name="name" class="form-control" />
        </div>
        <div class="form-group">
            <label>{{ __( 'Number' ) }}</label>
            <input type="text" required="" name="number" value="{{ old( 'number' ) }}" class="form-control" />
        </div>
        <div class="form-group">
            <button class="btn btn-success btn-block">{{ __( 'Save number' ) }}</button>
        </div>
    </form>
</div>
@endsection
