@extends( 'layouts.app' )

@section( 'content' )
<div class="container">
    <h1 class="text-center">
        {{ __( 'Edit number' ) }}
    </h1>
    @include( 'partials.errors' )
    <form method="POST" action="{{ route( 'numbers.update' , $number -> id ) }}" class="form">
        @csrf
        {{ method_field( 'PUT' ) }}
        <div class="form-group">
            <label>{{ __( 'Name' ) }}</label>
            <input type="text" required="" autofocus="" value="{{ $number -> name }}" name="name" class="form-control" />
        </div>
        <div class="form-group">
            <label>{{ __( 'Number' ) }}</label>
            <input type="text" required="" name="number" value="{{ $number -> number }}" class="form-control" />
        </div>
        <div class="form-check form-group">
            <input class="form-check-input" type="checkbox" {{ $number -> active ? "checked" : "" }} name="active" id="is_active">
            <label class="form-check-label" for="is_active">
                {{ __( 'Active' ) }}
            </label>
        </div>
        <div class="form-group">
            <button class="btn btn-success btn-block">{{ __( 'Save number' ) }}</button>
        </div>
    </form>
</div>
@endsection
