@extends( 'layouts.app' )

@section( 'content' )
<div class="container">
    <h1 class="text-center">
        {{ __( 'Numbers' ) }}
    </h1>
    <a class="btn btn-primary mb-2 mb-md-4" href="{{ route( 'numbers.create' ) }}">
        {{ __( 'Add number' ) }}
    </a>
    <table class="table-striped table-bordered table">
        <thead>
            <tr>
                <th>{{ __( 'ID' ) }}</th>
                <th>{{ __( 'Name' ) }}</th>
                <th>{{ __( 'Number' ) }}</th>
                <th>{{ __( 'Active' ) }}</th>
                <th>{{ __( 'Created at' ) }}</th>
                <th><i class="fas fa fa-wrench"></i></th>
            </tr>
        </thead>
        <tbody>
            @foreach ( $numbers as $number )
                <tr>
                    <td>{{ $number -> id }}</td>
                    <td>{{ $number -> name }}</td>
                    <td>{{ $number -> number }}</td>
                    <td>{{ $number -> is_active }}</td>
                    <td>{{ $number -> created_at }}</td>
                    <td>
                        <a class="btn btn-sm btn-warning" href="{{ route( 'numbers.edit' , $number -> id ) }}">{{ __( 'Edit' ) }}</a>
                        <a class="btn btn-sm btn-danger" href="{{ route( 'numbers.delete' , $number -> id ) }}" onclick="return confirm( 'Вы уверены?' );">{{ __( 'Delete' ) }}</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
