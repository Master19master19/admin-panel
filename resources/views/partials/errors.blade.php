@if( $errors -> any() )
   @foreach ($errors->all() as $error)
      <div class="text-danger alert alert-danger">{{ $error }}</div>
  @endforeach
@endif
