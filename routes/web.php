<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get( '/' , 'NumberController@index' );
Auth::routes();
Route::resource( 'numbers' , 'NumberController' );
Route::get( '/numbers/{id}/delete' , 'NumberController@destroy' ) -> name( 'numbers.delete' );